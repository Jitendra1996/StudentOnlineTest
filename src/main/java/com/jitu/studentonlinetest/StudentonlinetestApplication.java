package com.jitu.studentonlinetest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentonlinetestApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentonlinetestApplication.class, args);
	}

}
